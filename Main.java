package com.greatLearning.assignment2;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
ArrayList<Employee> employees = new ArrayList<>();
		
		//Setting values 
		Employee e1=new Employee();
		e1.setId(1);
		e1.setName("Aman");
		e1.setAge(20);
		e1.setSalary(1100000);
		e1.setDepartment("IT");
		e1.setCity("Delhi");
		
		//Setting values 
		Employee e2=new Employee();
		e2.setId(2);
		e2.setName("Bobby");
		e2.setAge(22);
		e2.setSalary(500000);
		e2.setDepartment("HR");
		e2.setCity("Bombay");
		
		//Setting values 
		Employee e3=new Employee();
		e3.setId(3);
		e3.setName("Zoe");
		e3.setAge(20);
		e3.setSalary(750000);
		e3.setDepartment("Admin");
		e3.setCity("Delhi");
		
		//Setting values 
		Employee e4=new Employee();
		e4.setId(4);
		e4.setName("Smitha");
		e4.setAge(21);
		e4.setSalary(1000000);
		e4.setDepartment("IT");
		e4.setCity("Chennai");
		
		//Setting values 
		Employee e5=new Employee();
		e5.setId(5);
		e5.setName("Smitha");
		e5.setAge(24);
		e5.setSalary(120000);
		e5.setDepartment("HR");
		e5.setCity("Benguluru");

		//adding employee objects to array list
		employees.add(e1);
		employees.add(e2);
		employees.add(e3);
		employees.add(e4);
		employees.add(e5);
		
		System.out.println("Employee Details------->");
		System.out.println("EmpId"+" 		"+"EmpName"+" 	"+"EmpAge"+" 		"+"EmpDept"+" 	"+"EmpSalary"+" 	"+"EmpCity");
		for(Employee e : employees) {
			try {
				if((e.getId()<0) || (e.getAge()<0) || (e.getSalary()<0)|| (e.getCity()==null) || (e.getDepartment()==null) || (e.getName()==null)) {
					throw new IllegalArgumentException();
				}
					
			}
			catch(Exception ex){
					System.out.println(ex);
			}
			System.out.println(e.getId()+"		"+e.getName()+"		"+e.getAge()+"		"+e.getDepartment()+"		"+e.getSalary()+"		"+e.getCity());
		}
		
		System.out.println("SORTING EMPLOYEE NAMES------->");
		EmployeeNameSort em=new EmployeeNameSort();
		em.sortingNames(employees);
		
		System.out.println("CALCULATING CITY COUNT------->");
		CityCount c=new CityCount();
		c.cityNameCount(employees);
		
		System.out.println("CALCULATING MONTHLY SALARY------->");
		MonthSalary ms=new MonthSalary();
		ms.monthlySalary(employees);
	}

}
