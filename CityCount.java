package com.greatLearning.assignment2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.TreeMap;

public class CityCount {

	public void cityNameCount(ArrayList<Employee> employees) { 
		ArrayList<String> city = new ArrayList<>();
		city.add(employees.get(0).getCity());
		city.add(employees.get(1).getCity());
		city.add(employees.get(2).getCity());
		city.add(employees.get(3).getCity());
		city.add(employees.get(4).getCity());
	
	
		HashSet<String> cityname = new HashSet<String>(city);
		TreeMap<String,Integer> tree=new  TreeMap<String,Integer> ();  
		for(String cities: cityname) {
			tree.put(cities,Collections.frequency(city, cities) );  
		}
		System.out.println(tree);
	}
}
