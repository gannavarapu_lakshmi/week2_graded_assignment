package com.greatLearning.assignment2;

import java.util.ArrayList;
import java.util.HashMap;


public class MonthSalary {

	 public void monthlySalary(ArrayList<Employee> employees) {
		 
		 HashMap<Integer,Float> sal=new HashMap<>();
		 sal.put(employees.get(0).getId(), (float) (employees.get(0).getSalary()/12));
		 sal.put(employees.get(1).getId(),(float)(employees.get(1).getSalary()/12));
		 sal.put(employees.get(2).getId(),(float)(employees.get(2).getSalary()/12));
		 sal.put(employees.get(3).getId(),(float)(employees.get(3).getSalary()/12));
		 sal.put(employees.get(4).getId(),(float)(employees.get(4).getSalary()/12));
		 
		 System.out.println(sal);
		 
	 }
	
}
